package no.ntnu.bachelor.voicepick.dtos;

import lombok.Data;

@Data
public class LocationEntityDto {

  private Long id;
  private LocationDto location;

}
