package no.ntnu.bachelor.voicepick.features.authentication.dtos;

import lombok.Data;

@Data
public class ProfilePictureDto {
    private String pictureName;

}
