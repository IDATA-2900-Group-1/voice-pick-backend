package no.ntnu.bachelor.voicepick.features.authentication.exceptions;

public class ResetPasswordException extends Exception {

    public ResetPasswordException(String msg) {
        super(msg);
    }

}
