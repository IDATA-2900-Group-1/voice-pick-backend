package no.ntnu.bachelor.voicepick.features.authentication.exceptions;

public class InvalidPasswordException extends Exception {

    public InvalidPasswordException(String msg) {
        super(msg);
    }

}
