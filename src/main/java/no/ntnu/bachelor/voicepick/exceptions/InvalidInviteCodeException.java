package no.ntnu.bachelor.voicepick.exceptions;

public class InvalidInviteCodeException extends Exception {

    public InvalidInviteCodeException(String msg) {
        super(msg);
    }

}
