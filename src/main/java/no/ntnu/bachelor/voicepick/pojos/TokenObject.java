package no.ntnu.bachelor.voicepick.pojos;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class TokenObject {

    private String token;

}
